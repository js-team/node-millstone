millstone(1) -- localize carto mml project files
================================================

SYNOPSIS
--------

    millstone [-n] <source mml file>
    millstone -h

DESCRIPTION
-----------

millstone localizes datasources found in a carto mml project file
before it is converted into a xml stylesheet usable by mapnik.

OPTIONS
-------

* `-n`  
  Boolean, use unmodified paths instead of symlinking files
* `-h`  
  Show help

SEE ALSO
--------

* carto(1)
* mapnik-inspect(1)
* mapnik-render(1)

